﻿namespace FirstProj
{
    class Program
    {
        static void Main()
        {
            try
            {
                Console.WriteLine("Nhap so giao vien: ");
                var num = int.Parse(Console.ReadLine() ?? "0");
                if (num <= 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(num));
                }

                var giaoViens = new GiaoVien[num];
                for (var i = 0; i < num; i++)
                {
                    Console.WriteLine($"Nhap thong tin giao vien thu {i + 1}: ");
                    Console.WriteLine("Nhap ho ten: ");
                    var hoTen = Console.ReadLine() ?? "";
                    Console.WriteLine("Nhap nam sinh: ");
                    var namSinh = int.Parse(Console.ReadLine() ?? "0");
                    Console.WriteLine("Nhap luong co ban: ");
                    var luongCoBan = int.Parse(Console.ReadLine() ?? "0");
                    Console.WriteLine("Nhap he so luong: ");
                    var heSoLuong = float.Parse(Console.ReadLine() ?? "0");
                    giaoViens[i] = new GiaoVien();
                    giaoViens[i].NhapThongTin(hoTen, namSinh, luongCoBan);
                    giaoViens[i].NhapThongTin(heSoLuong);
                }

                var giaoVienInfo = giaoViens.MinBy(gv => gv.TinhLuong());
                if (giaoVienInfo is not null)
                {
                    Console.WriteLine("Giao vien co luong thap nhat la: ");
                    giaoVienInfo.XuatThongTin();
                }

                Console.WriteLine("Nhan Enter de tiep tuc...");
                Console.ReadKey();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Environment.Exit(1);
            }
        }
    }

    class NguoiLaoDong
    {
        protected string _hoTen;
        protected int _namSinh;
        protected int _luongCoBan;

        public string HoTen
        {
            get => _hoTen;
            set => _hoTen = value;
        }

        public int NamSinh
        {
            get => _namSinh;
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentException("Nam sinh phai lon hon 0");
                }

                _namSinh = value;
            }
        }

        public int LuongCoBan
        {
            get => _luongCoBan;
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentException("Luong co ban phai lon hon 0");
                }

                _luongCoBan = value;
            }
        }

        public NguoiLaoDong() => _hoTen = "";

        public NguoiLaoDong(string hoTen, int namSinh, int luongCoBan)
        {
            HoTen = hoTen;
            _hoTen = hoTen;
            NamSinh = namSinh;
            LuongCoBan = luongCoBan;
        }

        public void NhapThongTin(string hoTen, int namSinh, int luongCoBan)
        {
            HoTen = hoTen;
            NamSinh = namSinh;
            LuongCoBan = luongCoBan;
        }

        public int TinhLuong() => LuongCoBan;

        public void XuatThongTin() =>
            Console.WriteLine($"Ho ten la: {HoTen}, nam sinh: {NamSinh}, luong co ban: {LuongCoBan}.");
    }

    class GiaoVien : NguoiLaoDong
    {
        protected float _hesoLuong;

        public float HeSoLuong
        {
            get => _hesoLuong;
            set => _hesoLuong = value;
        }

        public GiaoVien()
        {
            NamSinh = 1900;
            LuongCoBan = 0;
            HeSoLuong = 1.0f;
        }

        public GiaoVien(string hoTen, int namSinh, int luongCoBan, float heSoLuong)
        {
            HoTen = hoTen;
            NamSinh = namSinh;
            LuongCoBan = luongCoBan;
            HeSoLuong = heSoLuong;
        }

        public void NhapThongTin(float heSoLuong)
        {
            HeSoLuong = heSoLuong;
        }

        public new float TinhLuong() => LuongCoBan * HeSoLuong * 1.5f;

        public new void XuatThongTin()
        {
            var msg = $"{HoTen}, {NamSinh}, {LuongCoBan}, {HeSoLuong}, {TinhLuong()}";
            Console.WriteLine(msg);
        }

        public float XuLy()
        {
            return HeSoLuong + 0.6f;
        }
    }
}